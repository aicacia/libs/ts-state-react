# ts-state-react

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-state-react/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/state-react)](https://www.npmjs.com/package/@aicacia/state-react)
[![pipelines](https://gitlab.com/aicacia/libs/ts-state-react/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-state-react/-/pipelines)

connect react components with @aicacia/state stores